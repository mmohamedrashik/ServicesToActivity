package guy.droid.com.servicetoactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
TextView textView;
Button button,button2;
MyReciever reciever;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.start);
        button2 = (Button)findViewById(R.id.stop);
        textView = (TextView)findViewById(R.id.result);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reciever = new MyReciever();
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(MyService.MY_ACTION);
                registerReceiver(reciever, intentFilter);

                startService(new Intent(getApplicationContext(), MyService.class));
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(getApplicationContext(),MyService.class));
            }
        });
    }
    class MyReciever extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            String data = intent.getStringExtra("datas");
            textView.setText(data);
        }
    }
}
