package guy.droid.com.servicetoactivity;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

/**
 * Created by RASHIK on 1/25/2016.
 */
public class MyService extends Service {
    final static String MY_ACTION = "MY_ACTION";
    public static final long NOTIFY_INTERVAL = 1 * 1000; // 10 seconds
    android.os.Handler handler = new android.os.Handler();
    java.util.Timer timer = null;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       // Toast.makeText(getApplicationContext(),"STARTED",Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);

    }
    @Override
    public void onCreate() {
        super.onCreate();
        if(timer!=null)
        {
            timer.cancel();
        }
        else
        {
            timer = new java.util.Timer();
        }
        timer.scheduleAtFixedRate(new Timer(), 0, NOTIFY_INTERVAL);
    }

    @Override
    public void onDestroy() {
       // Toast.makeText(getApplicationContext(),"STOPPED",Toast.LENGTH_SHORT).show();
        super.onDestroy();
        timer.cancel();
    }
    class Timer extends TimerTask {

        @Override
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(getApplicationContext(),"RUNNING",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    intent.setAction(MY_ACTION);
                    intent.putExtra("datas", getDateTime());
                    sendBroadcast(intent);
                }
            });
        }
        private String getDateTime() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("[yyyy/MM/dd - HH:mm:ss]");
            return simpleDateFormat.format(new Date());
        }
    }
}
